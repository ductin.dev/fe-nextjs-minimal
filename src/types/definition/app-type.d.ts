// React
declare module 'react-dom';
declare module 'react-dom/client';
declare module 'react-dom/server';
declare module '@nextui-org/react';

// Style
declare module 'styled-components';

// Image
declare module '*.scss';
declare module "*.png";
declare module "*.jpg";
declare module "*.csv";

import { ReactNode } from "react";

export type TNextImageMinimal = {
  src: string,
  alt: string
}

export interface ICommonReactFC {
}

export interface IAppContainerReactFC {
  children: ReactNode
}
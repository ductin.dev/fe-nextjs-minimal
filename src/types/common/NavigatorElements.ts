export enum ENavigatorIndicator {
    HOME,
    COMMING_SOON,
    PAID_SOURCE,
    FREE_SOURCE,
    PROFILE,
    GALLERY,
    WIFI,
    VIP
}

export type TNavigatorElement = {
    indicator: ENavigatorIndicator,
    title: string,
    path: string,
    follow?: boolean,
    asPath?: string,
    target?: string
}

const NAVIGATOR_LIST: TNavigatorElement[] = [
    {
        indicator: ENavigatorIndicator.HOME,
        title: "Home Page",
        path: "/"
    },
    {
        indicator: ENavigatorIndicator.COMMING_SOON,
        title: "Comming Soon",
        path: "/comming-soon",
        follow: true,
    },
    {
        indicator: ENavigatorIndicator.PAID_SOURCE,
        title: "VIP Sources",
        path: "/paid-sources",
        follow: true,
    },
    {
        indicator: ENavigatorIndicator.FREE_SOURCE,
        title: "Free Demos",
        path: "/free",
        follow: true,
    },
    {
        indicator: ENavigatorIndicator.GALLERY,
        title: "Gallery Dashboard",
        path: "/gallery"
    },
    {
        indicator: ENavigatorIndicator.WIFI,
        title: "WIFI Miễn Phí",
        path: "/wifi",
        follow: true,
    },
    {
        indicator: ENavigatorIndicator.VIP,
        title: "TU-HOANG",
        path: "/tu-hoang"
    },
]

export default NAVIGATOR_LIST;
export type TRandomImageResponse = {
  src: string,
  alt: string,
  date_created: string
}
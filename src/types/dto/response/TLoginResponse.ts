export enum TUserRole { "USER", "MOD", "ADMIN" };

export type TLoginResponse = {
  user: string,
  role: TUserRole,
  token: string
}
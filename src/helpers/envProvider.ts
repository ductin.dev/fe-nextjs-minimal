export const APP_DOMAIN = process.env.NEXT_PUBLIC_APP_DOMAIN + "";
export const API_ENDPOINT = process.env.NEXT_PUBLIC_API_ENDPOINT + "";
export const TEST_ENDPOINT = process.env.NEXT_PUBLIC_TEST_ENDPOINT + "";

// IDENTITY
export const USER_LOGIN = API_ENDPOINT + "/login";
export const USER_DETAIL = API_ENDPOINT + "/user/detail";
export const USER_REGISTER = API_ENDPOINT + "/register";
export const USER_LOGOUT = API_ENDPOINT + "/logout";

// EXAM
export const USER_PROFILE = API_ENDPOINT + "/connect/token";

// TESTING
export const TEST_GG_DRIVE = TEST_ENDPOINT + "/google-drive/get-random";
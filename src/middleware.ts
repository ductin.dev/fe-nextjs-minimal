import { NextRequest, NextResponse } from "next/server";
import requestLogger from "@adapter/logger/requestLogger";

export default async function middleware(request: NextRequest) {
  const response = NextResponse.next();

  // Logger Middleware
  // requestLogger(request, response);

  return response;
}

export const config = {
  matcher: [
    /*
     * Match all request paths except for the ones starting with:
     * - _next/static (static files)
     * - _next/image (image optimization files)
     * - favicon.ico (favicon file)
     */
    '/((?!_next/static|_next/image|favicon.ico).*)',
  ],
};
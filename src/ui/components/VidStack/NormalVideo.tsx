'use client';

import { FC } from 'react';

import { MediaOutlet, MediaPlayer } from '@vidstack/react';
import { ICommonReactFC } from '@datatype/common/ComponentTypes';

import 'vidstack/styles/defaults.css';
import styles from './style.module.scss';

const NormalVideo: FC<ICommonReactFC> = () => {

  return (
    <MediaPlayer
      title="Agent 327: Operation Barbershop"
      src="https://media-files.vidstack.io/720p.mp4"
      poster="https://media-files.vidstack.io/poster.png"
      style={{ height: "650px", width: "800px" }}
    >
      <MediaOutlet />
    </MediaPlayer>
  );
}

export default NormalVideo;
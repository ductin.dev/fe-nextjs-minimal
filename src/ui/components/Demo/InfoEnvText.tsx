'use client';

import { FC, useEffect, useState } from "react";
import Image from "next/image";

import { API_ENDPOINT, USER_DETAIL } from "helpers/envProvider";
import { getRandomImage } from "@service/TestService/gg-drive";
import { ICommonReactFC, TNextImageMinimal } from "@datatype/common/ComponentTypes";
import { useClientReady } from "@hooks/useClientReady";

import styles from './style.module.scss';

const InfoEnvText: FC<ICommonReactFC> = () => {

  const clientReady = useClientReady();
  const [imageData, setImageData] = useState<TNextImageMinimal>({
    src: "",
    alt: ""
  });
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    getRandomImageHandler();
  }, []);

  const getRandomImageHandler = async () => {
    setLoading(true);
    const res = await getRandomImage();

    if (!res.error) {
      setImageData({ src: res.data.src, alt: res.data.alt })
    } else {
      console.log(res.error)
    }
    setLoading(false);
  }

  return (
    <>
      {clientReady && <div>
        Dashboard Page, mode: {process.env.NEXT_PUBLIC_ENVIROMENT}, path: {USER_DETAIL}, env: {API_ENDPOINT}
      </div>}
      <br></br>
      <button onClick={getRandomImageHandler}>Get from API (not yet)</button>
      <Image src={imageData.src} alt={imageData.alt} />
    </>
  );
}

export default InfoEnvText;

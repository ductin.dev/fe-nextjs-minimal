
'use client';

import { FC } from "react";

import { useClientReady } from "@hooks/useClientReady";
import { useAppDispatch, useAppSelector } from "@hooks/useRedux";
import { grantData, useData } from "@adapter/redux/action/TemplateAction";
import { ICommonReactFC } from "@datatype/common/ComponentTypes";

const ReduxTest: FC<ICommonReactFC> = () => {

  const dispatch = useAppDispatch();
  const clientReady = useClientReady();
  const { data } = useAppSelector((state) => state.templateReducer);

  return (
    <>
      Home Page, data in redux store: {clientReady && data}
      <br></br>
      Test Redux:
      <button onClick={() => grantData(1, dispatch)}>+</button>
      <button onClick={() => useData(1, dispatch)}>-</button>
      <br></br>
    </>
  );
}

export default ReduxTest;

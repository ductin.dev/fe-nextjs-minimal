import React, { FC } from 'react';

import { ICommonReactFC } from '@datatype/common/ComponentTypes';
import styles from './style.module.scss';

const Info401: FC<ICommonReactFC> = () => {

  return (
    <main className={styles.empty_body}>
    </main>
  );
};

export default Info401;
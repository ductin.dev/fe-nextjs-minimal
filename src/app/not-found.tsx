import { FC } from "react";

import { ICommonReactFC } from "@datatype/common/ComponentTypes";
import CodeXanh from "@assets/img/shape/CodeXanh.png";

import styles from "./style.module.scss";
import Link from "next/link";

const NotFound: FC<ICommonReactFC> = () => {

  return (
    <main>
      <section className={styles.page_404}>
        <div className="container mx-auto px-4">
          <div className="row">
            <div className="col_sm_12">
              <div className="col_sm_10 col_sm_offset_1 text_center">
                <div className={styles.four_zero_four_bg} style={{ backgroundImage: CodeXanh.src }}>
                  <h1 className={`text-center ${styles.simple_font}`}>
                    404 Not Found
                  </h1>
                </div>
              </div>
              <h3 className="text-center" style={{ fontWeight: 800 }}>
                Bọn mình không thể tìm thấy trang này, hoặc có thể bạn không
                có quyền truy cập{" "}
                <Link href="/">
                  <span className={styles.simple_text}>về trang chủ</span>
                </Link>
              </h3>
            </div>
          </div>
          <div className="row">
            <div className="col-12"></div>
          </div>
        </div>
      </section>
    </main>
  );
}

export default NotFound;

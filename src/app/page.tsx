import { FC } from "react";
import Image from "next/image";

import { ICommonReactFC } from "@datatype/common/ComponentTypes";
import bia_og_image from "@assets/img/others/bia.jpg"

import styles from "./style.module.scss";

export const metadata = {
  title: "WIFI MIỄN PHÍ",
  description: "Satdevelop cung cấp wifi miễn phí",
  openGraph: {
    images: [{
      url: bia_og_image.src
    }],
    title: "WIFI MIỄN PHÍ",
    description: "Satdevelop cung cấp wifi miễn phí"
  }
};

const TuHoangPage: FC<ICommonReactFC> = () => {

  return (
    <main>
      <h1 className="font-medium text-2xl">WIFI MIỄN PHÍ (Băng thông mạnh) cho các bạn ở <span style={{ color: "red" }}>106 - Sơn Thuỷ Đông 3</span></h1>
      <br></br>
      <p className={styles.text_container}>
        Đây là vị trí đặt server của cộng đồng satdevelop, băng tần phải khá mạnh và cần độ bảo mật cao nên chúng mình có lắp riêng
        WIFI để duy trì trạng thái 24/7 của server
      </p>
      <br></br>
      <h2><b>➡️ Để truy cập WIFI:</b></h2>
      <p className={styles.text_container}>
        <li>✅ Nếu bạn là sinh viên FPT: Hãy liên hệ Đoàn Đức Tín (admin) tại <a className={styles.gradient_text} href="https://www.facebook.com/satfomacej/" target="_blank">https://www.facebook.com/satfomacej/</a> để được <b>truy cập miễn phí</b>
        </li>
        <li>✅ Nếu bạn ở toà nhà này: Hãy tạo tài khoản tại blog<a className={styles.gradient_text} href="https://www.satdevelop.com/" target="_blank">Satdevelop</a> và làm theo hướng dẫn
        </li>
      </p>
      <br></br>
      <br></br>
      <Image src={bia_og_image.src} alt="Đoàn Đức Tín và bạn Satdevelop - Potoro Capstone - FPT FU Da Nang" />
    </main >
  );
}

export default TuHoangPage;
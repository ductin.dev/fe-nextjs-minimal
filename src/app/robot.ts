import { MetadataRoute } from 'next';

import { APP_DOMAIN } from 'helpers/envProvider';

export default function robots(): MetadataRoute.Robots {
  return {
    rules: {
      userAgent: '*',
      allow: '/',
    },
    sitemap: `${APP_DOMAIN}/sitemap.xml`,
  };
}
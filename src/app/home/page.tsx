import { FC } from "react";

import { ICommonReactFC } from "@datatype/common/ComponentTypes";
import ReduxTest from "@ui/components/Demo/ReduxTest";
import NormalVideo from "@ui/components/VidStack/NormalVideo";
import InfoEnvText from "@ui/components/Demo/InfoEnvText";

export const metadata = {
  title: "Home page | Expert Templates",
  description: "Home page, management your products here",
};

const HomePage: FC<ICommonReactFC> = () => {

  return (
    <main>
      <h2>
        Vì sản phẩm <b> EZCapstone -(Source code & Hiring platform for develop capstone, business projects & website) </b><br></br>
        đang trong giai đoạn thử nghiệm cho đến ngày 01/11/2023. Đây chỉ là trang để kiểm tra một số tính năng
      </h2>
      <br>
      </br>
      <InfoEnvText />
      <ReduxTest />
      <NormalVideo />
    </main>
  );
}

export default HomePage;

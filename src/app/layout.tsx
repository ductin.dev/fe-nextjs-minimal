import "@assets/../global.scss";

import AppWrapper from "@adapter/Wrapper";
import GuardMiddleware from "@adapter/guard/GuardMiddleware";
import ReduxMiddleware from "@adapter/redux/ReduxMiddleware";
import { APP_DOMAIN } from "helpers/envProvider";
import og_image from "@assets/img/ogbasic.png";

export const metadata = {
  title: {
    default: "Welcome to satdevelop.com",
    template: "%s | Expert Templates"
  },
  description: "Demo project and code templates, sharing source for your first product, paid for more. Provide by Doan Duc Tin on satdevelop.com",
  metadataBase: new URL(APP_DOMAIN),
  referrer: 'origin-when-cross-origin',
  authors: [{ name: 'Doan Duc Tin' }],
  keywords: ['Source Code', 'Next.js', 'React', 'HTML', 'JavaScript', 'FPT', 'Demo Code', 'Developer'],
  openGraph: {
    title: {
      default: "Welcome to satdevelop.com",
      template: "%s | Expert Templates"
    },
    description: "Demo project and code templates, sharing source for your first product, paid for more. Provide by Doan Duc Tin on satdevelop.com",
    images: [{
      url: og_image.src
    }],
    url: APP_DOMAIN
  }
};

export default function RootLayout({ children }: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body suppressHydrationWarning={true} >
        <AppWrapper
          InnerApp={children}
          OuterApp={() => <></>}
          adapters={[
            GuardMiddleware,
            ReduxMiddleware
          ]}
        />
      </body>
    </html>
  );
}

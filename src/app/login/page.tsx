import { FC } from "react";

import { ICommonReactFC } from "@datatype/common/ComponentTypes";

export const metadata = {
  title: "Login / Register",
  description: "Login and checkout some basic Demo Projects and Code",
};

const LoginPage: FC<ICommonReactFC> = () => {

  return (
    <main>
      Login Page
    </main>
  );
}

export default LoginPage;

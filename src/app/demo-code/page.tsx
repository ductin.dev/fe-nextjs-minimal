import { FC } from "react";

import InfoEnvText from "@ui/components/Demo/InfoEnvText";
import { ICommonReactFC } from "@datatype/common/ComponentTypes";

export const metadata = {
  title: "Dashboard",
  description: "Front-end by nextjs 13",
  openGraph: {
    title: "Dashboard",
    description: "Front-end by nextjs 13"
  }
};

const DemoCodePage: FC<ICommonReactFC> = () => {

  return (
    <main>
      <h2 className="font-medium text-2xl">Comming soon - FREE Source code & Template + Instructor</h2>
      <br></br>
      Chúng mình đang phát triển tính năng này và sẽ thông báo chính thức vào 01/11/2023
    </main >
  );
}

export default DemoCodePage;

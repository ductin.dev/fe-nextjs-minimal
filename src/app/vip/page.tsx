import { FC } from "react";
import Image from "next/image";
import { ICommonReactFC } from "@datatype/common/ComponentTypes";

import bia_image from "@assets/img/others/xa-may-chay-bang-bia.jpg";
import bia_og_image from "@assets/img/others/bia.jpg";

export const metadata = {
  title: "Biết thế nào là đủ là người giàu; biết gắng sức là người có chí. Kẻ nào không rời bỏ những điều trên thì sẽ được lâu dài, chết mà không mất là trường thọ, hồn nhiên vô tư",
  description: "Bia là bạn, không phải thù",
  openGraph: {
    images: [{
      url: bia_og_image.src
    }],
    title: "Biết thế nào là đủ là người giàu; biết gắng sức là người có chí. Kẻ nào không rời bỏ những điều trên thì sẽ được lâu dài, chết mà không mất là trường thọ, hồn nhiên vô tư",
    description: "Bia là bạn, không phải thù"
  }
};

const TuHoangPage: FC<ICommonReactFC> = () => {

  return (
    <main>
      <h2 className="font-medium text-2xl">Mỹ: Phát minh xe máy chạy bằng… bia</h2>
      <br></br>
      Ky Michaelson, một người Mỹ sống ở Michigan, đã sáng chế ra nhiều thứ kỳ lạ, như bồn cầu chạy bằng tên lửa hay máy cà phê chạy bằng động cơ phản lực, giờ thì phát minh ra xe máy chạy bằng bia, dù ông không hề uống giọt bia nào.
      <Image width={800} height={500} src={bia_image.src} alt={""} />
    </main >
  );
}

export default TuHoangPage;

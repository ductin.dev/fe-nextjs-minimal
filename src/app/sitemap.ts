import { MetadataRoute } from 'next';

import { APP_DOMAIN } from 'helpers/envProvider';

export default function sitemap(): MetadataRoute.Sitemap {
  return [
    {
      url: APP_DOMAIN,
      lastModified: new Date(),
    },
    {
      url: `${APP_DOMAIN}/home`,
      lastModified: new Date(),
    },
    {
      url: `${APP_DOMAIN}/login`,
      lastModified: new Date(),
    },
    {
      url: `${APP_DOMAIN}/gallery`,
      lastModified: new Date(),
    },
    {
      url: `${APP_DOMAIN}/vip`,
      lastModified: new Date(),
    },
  ];
}
import { FC } from "react";
import Image from "next/image";
import { ICommonReactFC } from "@datatype/common/ComponentTypes";

import bia_image from "@assets/img/others/xa-may-chay-bang-bia.jpg";
import bia_og_image from "@assets/img/others/bia.jpg";

export const metadata = {
  title: "Biết thế nào là đủ là người giàu; biết gắng sức là người có chí. Kẻ nào không rời bỏ những điều trên thì sẽ được lâu dài, chết mà không mất là trường thọ, hồn nhiên vô tư",
  description: "Bia là bạn, không phải thù",
  openGraph: {
    images: [{
      url: bia_og_image.src
    }],
    title: "Biết thế nào là đủ là người giàu; biết gắng sức là người có chí. Kẻ nào không rời bỏ những điều trên thì sẽ được lâu dài, chết mà không mất là trường thọ, hồn nhiên vô tư",
    description: "Bia là bạn, không phải thù"
  }
};

const TuHoangPage: FC<ICommonReactFC> = () => {

  return (
    <main>
      <h2 className="font-medium text-2xl">Comming soon - Paid Source Code & Project</h2>
      <br></br>
      Chúng mình đang phát triển tính năng này và sẽ thông báo chính thức vào 01/11/2023
    </main >
  );
}

export default TuHoangPage;

import NAVIGATOR_LIST, { ENavigatorIndicator, TNavigatorElement } from "@datatype/common/NavigatorElements";

export const useNavigatorList = (
  navigatorIndicatorList: ENavigatorIndicator[]
) => {
  let result: TNavigatorElement[] = [];
  NAVIGATOR_LIST.forEach(item => {
    if (navigatorIndicatorList.includes(item.indicator)) {
      result.push(item);
    }
  })
  return result;
};

export const useActiveNavigatorElement = (
  path: string
) => {
  for (let i = 0; i < NAVIGATOR_LIST.length; i++) {
    if (NAVIGATOR_LIST[i].path == path) {
      return NAVIGATOR_LIST[i];
    }
  }
  return NAVIGATOR_LIST[0];
};
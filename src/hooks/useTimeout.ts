export const useTimeout = (
  immediateExecute: () => void,
  scheduleExecute: () => void,
  miliseconds: number,
) => {
  immediateExecute();
  const timeout = setTimeout(scheduleExecute, miliseconds);

  return () => clearInterval(timeout);
};
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'

import { RootStateType, DispatchType } from '@adapter/redux/stateProvider';

export const useAppDispatch: () => DispatchType = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootStateType> = useSelector;
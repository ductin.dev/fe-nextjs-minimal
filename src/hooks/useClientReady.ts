import { useState, useEffect } from "react";

export const useClientReady = (): boolean => {
  const [clientReady, setClientReady] = useState<boolean>(false);

  useEffect(() => {
    setClientReady(true);
  }, []);

  return clientReady;
};
import axios from "axios";

const defaultHeader = {
  "Content-Type": "application/json",
  Accept: "*/*",
}

export const useFetchData = (
  endPoint: string,
  method: string,
  options: {},
  haveAuthen: boolean = false,
  headers: {} = defaultHeader,
) => {
  const completeFeature = {
    isComplete: false,
    data: null as any,
    error: null as any
  }

  if (haveAuthen) {
    headers = { ...headers, Authorization: `Bearer ${localStorage.getItem("BEARER_TOKEN")}` }
  }

  const fetchData = async () => {
    try {
      const response = await axios.request({
        method: method,
        url: endPoint,
        headers: headers,
        ...options
      });

      completeFeature.data = response.data;
    } catch (error) {
      completeFeature.error = error;
    } finally {
      completeFeature.isComplete = true;
    }
  };

  return { completeFeature, fetchData };
};
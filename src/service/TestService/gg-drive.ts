import { TRandomImageResponse } from "@datatype/dto/response/TRandomImageResponse";
import { useFetchData } from "@hooks/useFetchData"
import { TEST_GG_DRIVE } from "helpers/envProvider"

export const getRandomImage = async () => {
  const {
    completeFeature: completeFeature,
    fetchData: fetchData
  } = useFetchData(TEST_GG_DRIVE, "GET", {
    body: {

    }
  });

  await fetchData();
  const data = completeFeature.data as TRandomImageResponse;
  const error = completeFeature.error;

  return { data, error };
}
import { TLoginResponse } from "@datatype/dto/response/TLoginResponse";
import { useFetchData } from "@hooks/useFetchData";
import { USER_LOGIN } from "helpers/envProvider";

export const login = async (username: string, password: string) => {

  const {
    completeFeature: completeFeature,
    fetchData: fetchData,
  } = useFetchData(USER_LOGIN, "POST", {
    body: {
      username: username,
      password: password
    }
  });

  await fetchData();
  const data = completeFeature.data as TLoginResponse;
  const error = completeFeature.error;

  // Save token local storage [For easy access]
  if (!error && data.token) {
    localStorage.setItem("BEARER_TOKEN", data.token);
  }

  return { data, error };
}
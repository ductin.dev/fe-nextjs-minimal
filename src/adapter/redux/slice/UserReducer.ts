import { createSlice } from '@reduxjs/toolkit';
import { TLoginResponse } from '@datatype/dto/response/TLoginResponse';

// Config Instance
export interface TUserState {
  user_info: TLoginResponse | null,
  is_login: boolean,
  api_token: string | null;
}

export const userInitialState: TUserState = {
  user_info: null,
  is_login: false,
  api_token: null
};

// Define Slice
export const slice = createSlice({
  name: 'User Slice',
  initialState: userInitialState,
  reducers: {
    authenCompleteSuccess: (state, action) => {
      state.api_token = action.payload;
    },
    setUserInfoSuccess: (state, action) => {
      state.user_info = action.payload;
      state.is_login = true;
    },
    logoutCompleteSuccess: (state) => {
      state = userInitialState;
    }
  },
});

export default slice.reducer;

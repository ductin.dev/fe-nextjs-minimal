import { createSlice } from '@reduxjs/toolkit';

// Config Instance
export interface TTemplateState {
  name: string,
  data: number,
};

export const initialState: TTemplateState = {
  name: "Unnamed User",
  data: 30
};

// Define Slice
export const slice = createSlice({
  name: 'Template Slice',
  initialState: initialState,
  reducers: {
    changeNameSuccess: (state, action) => {
      state.name = action.payload;
    },
    grantSuccess: (state, action) => {
      state.data = state.data + action.payload;
    },
    grantFailure: (state, action) => {
      console.error(action.payload);
    },
    substractSuccess: (state, action) => {
      state.data = state.data - action.payload;
    },
    substractFailure: (state, action) => {
      console.error(action.payload);
    }
  }
});

export default slice.reducer;
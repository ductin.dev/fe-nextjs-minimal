import { slice } from '../slice/TemplateReducer';
import { DispatchType } from '../stateProvider';

const { changeNameSuccess, substractSuccess,
  substractFailure, grantSuccess, grantFailure, } = slice.actions;

export const grantData = (amount: number, dispatch: DispatchType) => {
  try {
    return dispatch(grantSuccess(amount));
  } catch (e) {
    return dispatch(grantFailure({ e }));
  };
};

export const useData = (amount: number, dispatch: DispatchType) => {
  try {
    return dispatch(substractSuccess(amount));
  } catch (e) {
    return dispatch(substractFailure({ e }));
  };
};

export const changeName = (name: string, dispatch: DispatchType) => {
  try {
    return dispatch(changeNameSuccess(name));
  } catch (e) {
    //
  };
};

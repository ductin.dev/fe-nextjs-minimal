import { TLoginResponse } from '@datatype/dto/response/TLoginResponse'

import { slice } from '../slice/UserReducer';
import { DispatchType } from '../stateProvider';

const { authenCompleteSuccess, setUserInfoSuccess, logoutCompleteSuccess } = slice.actions;

export const authenComplete = (token: string, dispatch: DispatchType) => {
  try {
    return dispatch(authenCompleteSuccess(token));
  } catch (e) {
  };
};

export const setUserInfo = (userInfo: TLoginResponse, dispatch: DispatchType) => {
  try {
    return dispatch(setUserInfoSuccess(userInfo));
  } catch (e) {
  };
};

export const logoutComplete = (dispatch: DispatchType) => {
  try {
    return dispatch(logoutCompleteSuccess());
  } catch (e) {
  };
};

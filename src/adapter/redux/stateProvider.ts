
import thunk from 'redux-thunk';
import { CombinedState, combineReducers, Reducer } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { configureStore } from '@reduxjs/toolkit';

import TemplateReducer, { TTemplateState } from './slice/TemplateReducer';
import UserReducer, { TUserState } from './slice/UserReducer';

export interface RootStateType {
  templateReducer: TTemplateState;
  userReducer: TUserState;
}

// Config
const persistConfig = {
  key: 'user-store',
  storage
};

// Combine Reducer and save state
const reducers: Reducer<CombinedState<RootStateType>> = combineReducers({
  templateReducer: TemplateReducer,
  userReducer: UserReducer
});
const persistedReducer = persistReducer(persistConfig, reducers);

// Init Store
const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NEXT_PUBLIC_ENVIROMENT === 'DEV-MODE',
  middleware: [thunk]
});

export type DispatchType = typeof store.dispatch;
export const persistor = persistStore(store);
export default store;
"use client";

import React, { FC, PropsWithChildren, ReactNode } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { IAppContainerReactFC } from '@datatype/common/ComponentTypes';
import store, { persistor } from './stateProvider';

const ReduxMiddleware: FC<IAppContainerReactFC> = (props: PropsWithChildren<IAppContainerReactFC>) => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        {() => (
          props.children
        )}
      </PersistGate>
    </Provider>
  );
};

export default ReduxMiddleware;
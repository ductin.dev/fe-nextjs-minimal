import React, { FC, ReactNode } from "react";

import { IAppContainerReactFC } from "@datatype/common/ComponentTypes";

interface TOuterApp {
};

export interface IMiddlewareFactory {
  InnerApp: ReactNode;
  OuterApp: React.FC<TOuterApp>;
  adapters?: React.FC<IAppContainerReactFC>[];
};

const AppWrapper: FC<IMiddlewareFactory> = ({ InnerApp, OuterApp, adapters }) => {
  var App = InnerApp;

  adapters ? adapters.forEach((Component, index) => {
    App = <Component key={index}>
      {App}
    </Component>
  }) : <></>

  return (
    <>
      {App}
    </>
  );
};

export default AppWrapper;
export const NOT_REQUIRED_LOGIN_PATHS = ["/login", "register"];
export const HIDDEN_LAYOUT_PATHS = ["/login", "register"];
export const DISABLE_SCROLL_TO_TOP_PATHS = ["/home", "login", "register"];
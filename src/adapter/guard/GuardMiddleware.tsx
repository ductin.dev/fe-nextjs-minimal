"use client";

import { FC, PropsWithChildren, useEffect, useState } from "react";
import { usePathname, useRouter } from "next/navigation";

//Layout
import Header from "@ui/layout/Header/Header";
import Footer from "@ui/layout/Footer/Footer";
import Info401 from "@ui/components/FallBack/Info401";

import { useAppSelector } from "@hooks/useRedux";
import { TUserRole } from "@datatype/dto/response/TLoginResponse";
import { IAppContainerReactFC } from "@datatype/common/ComponentTypes";
import { DISABLE_SCROLL_TO_TOP_PATHS, HIDDEN_LAYOUT_PATHS, NOT_REQUIRED_LOGIN_PATHS } from "./routerConfig";
import { useClientReady } from "@hooks/useClientReady";
import { useActiveNavigatorElement } from "@hooks/useNavigator";


const GuardMiddleware: FC<IAppContainerReactFC> = (props: PropsWithChildren<IAppContainerReactFC>) => {

  const router = useRouter();
  const pathName = usePathname();

  const { user_info, is_login } = useAppSelector((state) => state.userReducer);
  const [isUsingHeaderFooter, setIsUsingHeaderFooter] = useState<boolean>(!HIDDEN_LAYOUT_PATHS.includes(pathName));
  const [isHavePermission, setIsHavePermission] = useState<boolean>(NOT_REQUIRED_LOGIN_PATHS.includes(pathName) || is_login);

  // Client Ready
  const isClientReady = useClientReady();

  // Get current tab (path)
  const activeTab = useActiveNavigatorElement(pathName);

  // Check permission
  useEffect(() => {
    setIsUsingHeaderFooter(!HIDDEN_LAYOUT_PATHS.includes(pathName));
    setIsHavePermission(NOT_REQUIRED_LOGIN_PATHS.includes(pathName) || is_login);
  }, [pathName, is_login]);

  useEffect(() => {
    if (!isHavePermission) {
      router.push("/login");
    }
  }, [isHavePermission, router])


  return <>
    <Header show={isUsingHeaderFooter} activeTab={activeTab}  />
    {isHavePermission && props.children}
    <Footer show={isUsingHeaderFooter} />
  </>
};

export default GuardMiddleware;

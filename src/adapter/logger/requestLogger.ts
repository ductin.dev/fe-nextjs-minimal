import { NextRequest, NextResponse } from "next/server";
import { getLogger } from "./log-util";

export default function requestLogger(request: NextRequest, response: NextResponse) {

  const time = Date.now();
  const timeStr = new Date(time).toLocaleDateString();

  const logger = getLogger("home");

  const logData = `[${timeStr}] Request Url: ${request.url} - HOST ${request.headers.get("Host")} - Agent: ${request.headers.get("User-Agent")}`;
  logger.info(logData)
}
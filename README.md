# FE-ClienModule
Client: ReactJS - NextJS

**Development Step**
+ Edit local config and enviroment 
variable (database, server..) at .env, .env.dev
+ Install yarn ```$ npm i -g yarn``
+ Run development enviroment
```
$ yarn install
$ yarn dev
```
======================================
* Reslove error if have in first step:
+ Delete package-lock.json and node_modules
+ Re-install above steps

**Install new packages and reslove dependencies**
```
$ yarn add <package_name>
```
* Fix conflict in yarn.lock (if have)

- For update specific version use:
```
$ yarn upgrade <package_name> --latest
```

**Run the module on Target Machine**
```
$ yarn build
$ yarn start
```
- Make sure you have create your .env.local on target machine cause it ignore by git
- Above command will use in target machine

# Coding convention
Make the code easier to review and maintain before creating new features

**Comment conventions**
- DUMMY DATA: ```// Dummy Data```
- EFFECT hooks (useEffect): ```// EFFECT: {Brief description this hook and what it do}```
- Complex component's functions: ```// FEAT: {Brief description this function and what it do}```
- TO DO: ```// TO-DO: {Brief define to do}```

**Naming conventions**
- .ts - helpers, library, commonjs files: ```camelCase.ts```  (```kebab-case.ts``` for /service folder)
- .ts - /service folder: ```kebab-case.ts```
- .ts - using as classes | Type, Interface, Enum containers: ```PascalCase.ts```
- .tsx files: ```PascalCase.tsx```

- /app folder: ```page.tsx```
- /container folder: ```___Container.tsx```
- /components folder: Using for common Components among app and should contains no hook

- Functions name: ```const handle___ = () => {}```
- /hooks folder: ```use___.ts```

- DTOs, scss classes, attribute: ```prefer snake_case, then camelCase```
- Types: ```T - prefix```
- Interfaces: ```I - prefix```
- Enums: ```E - prefix```

**File format**
- Tab size: 2 SPACES
- Import orders: /node_modules -> @our_modules -> ./relative-imports
- 1 Line break when: between *import types*, after *interface/class/function/type/enum* initiation, before *return/export* statements
- Also add *1 Line break* when the function is to complex, the code structure to too long,..etc